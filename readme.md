## Contents
1. anti-analysis: techniques observed in malware to thwart analysis
2. api-hooker: On-going project
3. ghidra-fids: Function IDs generated from analyzed samples
4. ghidra-scripts: Scripts from analyzed samples
5. pencil: Binary disassembler with select capabilites
6. yara-rulset: yara rules from worked cases
