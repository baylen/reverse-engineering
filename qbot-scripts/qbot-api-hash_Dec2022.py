#!/usr/bin/python3

'''
This script requires the ciphertext from the sample to be carved out and saved as a bin file. Usage is as such:

python api-decode.py <relevantdll.dll> <ciphertext.bin>
Example:
$ python api-decode.py kernel32.dll ct_kernel32.bin
kernel32.dll
API Name, Quakbot Hash
LoadLibraryA , 0x1e4e54d6
LoadLibraryW , 0xea9ae187
FreeLibrary , 0xfbe7cad4
   ....

As of Dec 2022, Qbot stores the "Qbot hash" in an array. This script iterates over every dword of the ciphertext and performs the hashing algorithm against all the exports in the relevent DLL. If there is a match then it prints the API and the Qbot hash to concole with a comma between them so the results can easily be used in another application of fed into another script.

'''

import pefile
import sys
import zlib
import struct
import sys

#knownHash comes from data blob, listOfKnownExport comes from parsed pemodule
def bruteforce_api_by_hash(knownHashes, listOfKnownExports):
    print("API Name, Quakbot Hash")
    for knownHash in knownHashes:
        for export in listOfKnownExports:
            crc = zlib.crc32(export.encode('utf8'))
            crc = (crc ^ 563079515)
            if crc == knownHash:
                print(export, ",", hex(crc))

def main():

    name = str(sys.argv[1])
    pe = pefile.PE(sys.argv[1])

    exports = []
    for export in pe.DIRECTORY_ENTRY_EXPORT.symbols:
        exports.append(export.name)

    # Get the name exports from legitimate Windows DLLs
    dexports = []
    for export in exports:
        if export == None:
            continue
        strExport = export.decode()
        dexports.append(strExport)

    # "brute force" the against the list of known hashes. While loop reads a dword at a time and increments and puts it into a list that can be thrown into a function
    knownHashes = []
    with open(sys.argv[2], 'rb') as f:
        data = f.read(4)
        while data:
            number = int.from_bytes(data, "little")
            knownHashes.append(number)
            data = f.read(4)

    f.close()

    print(name)
    bruteforce_api_by_hash(knownHashes, dexports)

if __name__ == '__main__':
    main()
