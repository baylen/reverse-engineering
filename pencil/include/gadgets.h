#ifndef GADGETS
#define GADGETS

int find_rop_gadget(Binary *bin, const char *_section);

int find_gadgets_at_root(Section *section, uint64_t root, std::map<std::string, std::vector<uint64_t> > *gadgets, csh disassembly);

#endif