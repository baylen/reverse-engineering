#include <stdio.h>
#include <map>
#include <vector>
#include <string>
#include <capstone/capstone.h>
#include "loader.h"
#include "disassembler.h"
#include "gadgets.h"
/*
* Add any other gadgets to this file as they come up. Currently this only has a ROP gadget.
*
*/

// TODO: Change this to add two binaries as input, have it run on both of them, and compare results and only display the gadgets that are present in both binary variants
int find_rop_gadget(Binary *bin, const char *_section) {
    csh disassembly;
    Section *section;
    std::map<std::string, std::vector<uint64_t> > gadgets;

    // x86 ret instruction.
    // TODO: This needs to be expanded on to support other architectures.
    const uint8_t x86_opc_ret = 0xc3;

    // Get metadata on binary from the loader to ease passing the right arguments to capstone functions:
    uint32_t bits = bin->bits;
    std::string arch_str = bin->arch_str;
    int arch = bin->arch; //To add more architecture support I need to add more to the Binary::BinaryArch enum types. 
    
    //printf("DEBUG FLAG: %d\n", arch);
    
    // Get the section to look for ROP gadgets
    section = bin->get_section(_section);
    if(!section) {
        fprintf(stderr, "[ !get_section()  ]: Failed to disassembler section\n");
        return 0;
    }

    if(bits == 64 && arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_64, &disassembly) != CS_ERR_OK) {
            fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
            return -1;
        }
    }
    else if(bits == 32 && arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_32, &disassembly) != CS_ERR_OK) {
                fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
                return -1;
        }
    }
 
    cs_option(disassembly, CS_OPT_DETAIL, CS_OPT_ON);

    /* Loop through all bytes individually to look for possible ret instruction and map
    * them all with find_gadgets_at_root() where gadgets points to (&gadgets)
    */ 
    for(size_t i = 0; i < section->size; i++) {
        if(section->bytes[i] == x86_opc_ret) {
            if(find_gadgets_at_root(section, section->vma+i, &gadgets, disassembly) < 0) {
                break;
            }
        }
    }

    /* A simple trick to get how many elemnts an array has. Maybe implement this later.
    short array[] = { 104, 105, 32, 98, 105, 108, 108, 0 };
    size_t n_elements = sizeof(array) / sizeof(short);
    */
    // Print out the mapped gadgets. Var gadgets is an array.
    for(auto &kv: gadgets) {
        printf("%s\t[ ", kv.first.c_str());
        for(auto addr: kv.second) {
            printf("0x%jx ", addr);
        }
        printf("]\n");
    }

    cs_close(&disassembly);

    return 0;
}
int find_gadgets_at_root(Section *section, uint64_t root, std::map<std::string, std::vector<uint64_t> > *gadgets, csh disassembly) {
    size_t n, len;
    const uint8_t *pc;
    uint64_t offset, addr;
    std::string gadget_str;
    cs_insn *cs_ins;

    const size_t max_gadget_len = 0x5; // TODO: Add this as function arg maybe???
    const size_t x86_max_ins_bytes = 0xf; //x86 instructions are never larger than 15 bytes
    const uint64_t root_offset = max_gadget_len*x86_max_ins_bytes;

    cs_ins = cs_malloc(disassembly);
    if(!cs_ins) {
        fprintf(stderr, "[  !cs_malloc() ]: Failed in capstone function\n");
        return -1;
    }

    for(uint64_t a = root-1; a >= root-root_offset && a >= 0; a--) {
        addr = a;
        offset = addr - section->vma;
        pc = section->bytes + offset;
        n = section->size - offset;
        len = 0;
        gadget_str = "";
        while(cs_disasm_iter(disassembly, &pc, &n, &addr, cs_ins)) {
            if(cs_ins->id == X86_INS_INVALID || cs_ins->size == 0) {
                break;
            } else if(cs_ins->address > root) {
                break;
            } else if(is_cs_cflow_instruction(cs_ins) && !is_cs_x86_ret_instruction(cs_ins)) {
                break;
            } else if(++len > max_gadget_len) {
                break;
            }

            gadget_str +=std::string(cs_ins->mnemonic) + " " + std::string(cs_ins->op_str);

            if(cs_ins->address == root) {
                (*gadgets)[gadget_str].push_back(a);
                break;
            }

            gadget_str += "; ";
        }
    }

    cs_free(cs_ins, 1);

    return 0;
}
