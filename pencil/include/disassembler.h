#ifndef DISASSEMBLE_UTILS
#define DISASSEMBLE_UTILS

void print_instruction(cs_insn *instruction);

bool is_cs_cflow_group(uint8_t group);

bool is_cs_cflow_instruction(cs_insn *instruction);

bool is_cs_unconditional_cflow_instruction(cs_insn *instruction);

bool is_cs_x86_ret_instruction(cs_insn *instruction);

uint64_t get_cs_ins_immediate_target(cs_insn *instruction);

int recursive_disassemble(Binary *bin, const char *_section);

int linear_disassemble(Binary *bin, const char *_section);

#endif
