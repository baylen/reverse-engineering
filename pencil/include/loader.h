#ifndef LOADER_H
#define LOADER_H

#include <stdint.h>
#include <string>
#include <vector>

#define bfd_get_section_flags(bfd, ptr) ((void) bfd, (ptr)->flags)

class Binary;
class Section;
class Symbol;

class Symbol {
public:
  enum SymbolType {
    SYM_TYPE_UKN    = 0,
    SYM_TYPE_FUNC   = 1,
    SYM_TYPE_LOCAL  = 2, // Added for exercise
    SYM_TYPE_GLOBAL = 3, // Added for exercise
    SYM_TYPE_WEAK   = 4
  };

  Symbol() : type(SYM_TYPE_UKN), name(), addr(0) {}

  SymbolType  type;
  std::string name;
  uint64_t    addr;
};

class Section {
public:
  enum SectionType {
    SEC_TYPE_NONE = 0,
    SEC_TYPE_CODE = 1,
    SEC_TYPE_DATA = 2
  };

  Section() : binary(NULL), type(SEC_TYPE_NONE), vma(0), size(0), bytes(NULL) {}

  bool contains (uint64_t addr) { return (addr >= vma) && (addr-vma < size); }

  Binary       *binary;
  std::string   name;
  SectionType   type;
  uint64_t      vma;
  uint64_t      size;
  uint8_t       *bytes;
};

class Binary {
public:
  enum BinaryType {
    BIN_TYPE_AUTO = 0,
    BIN_TYPE_ELF  = 1,
    BIN_TYPE_PE   = 2
  };
  /* TODO: Can add support for other architectures here if necessary. 
  * Bits and Arch can be set in loader.cpp bfd_get_arch_info() function in the following switch statement.
  * This loader is used mostly for windows PE files, so we didn't add support for other architectures here.
  */
  enum BinaryArch {
    ARCH_NONE     = 0,
    ARCH_X86      = 1,  
	  ARCH_ARM      = 2,	///< ARM architecture (including Thumb, Thumb-2)
	  ARCH_ARM64    = 3,	///< ARM-64, also called AArch64
	  ARCH_MIPS     = 4,  ///< Mips architecture
	  ARCH_PPC		  = 5,  ///< PowerPC architecture
	  ARCH_SPARC	  = 6,	///< Sparc architecture
	  ARCH_SYSZ 	  = 7,	///< SystemZ architecture
	  ARCH_XCORE	  = 8	  ///< XCore architecture
  };

  Binary() : type(BIN_TYPE_AUTO), arch(ARCH_NONE), bits(0), entry(0) {}


  //Use this default if you just want the text section
  Section *get_text_section() {
    for(auto &s : sections) {
        if(s.name == ".text") {
            return &s;
        }
    }
    return NULL;
  }

  /* Use this is you want another section. Example:
  *     Binary *bin;
  *     load_binary(fname, &bin, Binary::BIN_TYPE_AUTO);
  *
  *     const char *_section = ".text";
  *     section = bin->get_section(_section);
  */
  Section *get_section(const char *sname) {
    for(auto &s : sections) {
        if(s.name == sname) {
            return &s;
        }
    }
    return NULL;
  }


  std::string          filename;
  BinaryType           type;
  std::string          type_str;
  BinaryArch           arch;
  std::string          arch_str;
  unsigned             bits;
  uint64_t             entry;
  std::vector<Section> sections;
  std::vector<Symbol>  symbols;
};


int  load_binary   (std::string &fname, Binary *bin, Binary::BinaryType type);
void unload_binary (Binary *bin);

#endif /* LOADER_H */
