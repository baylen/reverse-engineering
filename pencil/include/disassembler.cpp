#include <stdio.h>
#include <map>
#include <queue>
#include <vector>
#include <string>
#include <capstone/capstone.h>
#include <loader.h>
#include "disassembler.h"

void print_instruction(cs_insn *instruction) {
    printf("0x%016jx: ", instruction->address);
    for(size_t i = 0; i < 16; i++) {
        if(i < instruction->size) printf("%02x ", instruction->bytes[i]);
        else printf("   ");
    }
    printf("%-12s %s\n", instruction->mnemonic, instruction->op_str);
}

bool is_cs_cflow_group(uint8_t group) {
    return (group == CS_GRP_JUMP) || 
           (group == CS_GRP_CALL) || 
           (group == CS_GRP_RET) || 
           (group == CS_GRP_IRET);
}

bool is_cs_cflow_instruction(cs_insn *instruction) {
    for(size_t i = 0; i < instruction->detail->groups_count; i++) {
        if(is_cs_cflow_group(instruction->detail->groups[i])) {
            return true;
        }
    }
    return false;
}

bool is_cs_unconditional_cflow_instruction(cs_insn *instruction) {
    switch(instruction->id) {
        case X86_INS_JMP:
        case X86_INS_LJMP:
        case X86_INS_RET:
        case X86_INS_RETF:
        case X86_INS_RETFQ:
            return true;
        default:
            return false;
    }
}

bool is_cs_x86_ret_instruction(cs_insn *instruction){
    switch(instruction->id) {
        case X86_INS_RET:
            return true;
        default:
            return false;
    }
}

uint64_t get_cs_ins_immediate_target(cs_insn *instruction) {
    cs_x86_op *cs_op;

    for(size_t i = 0; i < instruction->detail->groups_count; i++) {
        if(is_cs_cflow_group(instruction->detail->groups[i])) {
            for(size_t j = 0; j < instruction->detail->x86.op_count; j++) {
                cs_op = &instruction->detail->x86.operands[j];
                if(cs_op->type == X86_OP_IMM) {
                    return cs_op->imm;
                }
            }
        }
    }

    return 0;
}

int recursive_disassemble(Binary *bin, const char *_section) {
    csh disassembly;
    cs_insn *cs_instructions;
    Section *text;
    size_t n;
    const uint8_t *pc;
    uint64_t addr, offset, target, block_end_addr;
    std::queue<uint64_t> Q;
    std::map<uint64_t, bool> seen;
    std::vector<uint64_t> V, overlap;

    text = bin->get_section(_section);

    if(!text) {
        fprintf(stderr, "[ !get_section() ]: Failed to get .text section\n");
        return 0;
    }

    // Get metadata on binary from the loader to ease passing the right arguments to capstone functions:
    uint32_t bits = bin->bits;
    std::string arch_str = bin->arch_str;
    int arch = bin->arch; //To add more architecture support I need to add more to the Binary::BinaryArch enum types.

    if(bits == 64 && arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_64, &disassembly) != CS_ERR_OK) {
            fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
            return -1;
        }
    }
    else if(bits == 32 &&arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_32, &disassembly) != CS_ERR_OK) {
            fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
            return -1;
        }
    }

    cs_option(disassembly, CS_OPT_DETAIL, CS_OPT_ON);

    cs_instructions = cs_malloc(disassembly);
    if(!cs_instructions) {
        fprintf(stderr, "[ !cs_malloc() ]: Out of memory\n");
        return -1;
    }

    // Recursive disassembly is structured around a queue. Bootstrap main entry point and then the known function symbol entry points.
    addr = bin->entry;
    if(text->contains(addr)) Q.push(addr);
    printf("entry point: 0x%16jx\n", addr);

    for(auto &sym: bin->symbols) {
        if(sym.type == Symbol::SYM_TYPE_FUNC && text->contains(sym.addr)) {
            Q.push(sym.addr);
            printf("Function Symbol: 0x%016jx\n", sym.addr);
        }
    }

    while(!Q.empty()) {
        addr = Q.front();
        Q.pop();
        if(seen[addr]) {
            //printf("[!] Ignoring address 0x%016jx (already seen)\n");
            continue;
        }
    
        offset = addr - text->vma;
        pc = text->bytes + offset;
        n = text->size - offset;

        /* cs_disasm_iter() returns either true or false. Disasembles only one instruction at a time.
        * cs_disasm_iter() doesn't require an allocated buffer because it only does one instruction at a time.
        * Control could be added in this loop to "inspect" control flow.
        */
        printf("____________________________CODE_BLOCK_0x0%jx____________________________\n", addr);
        V.push_back(addr);
        while(cs_disasm_iter(disassembly, &pc, &n, &addr, cs_instructions)) {
            if(cs_instructions->id == X86_INS_INVALID || cs_instructions->size == 0) {
                break;
            }

            seen[cs_instructions->address] = true;
            print_instruction(cs_instructions);

            if(is_cs_cflow_instruction(cs_instructions)) {
                target = get_cs_ins_immediate_target(cs_instructions);
                overlap.push_back(target);
                if(target && !seen[target] && text->contains(target)) {
                    Q.push(target);
                    printf("  -> new target: 0x%016jx\n", target);
                }        
                if(is_cs_unconditional_cflow_instruction(cs_instructions)) {
                    break;
                }
            }
            else if(cs_instructions->id == X86_INS_HLT) {
                break;
            }
        }

        block_end_addr = addr - 1;
        V.push_back(block_end_addr);
        printf("\tEND_BLOCK_ADDRESS:_0x%jx\n", block_end_addr);
    }

    // https://cplusplus.com/reference/vector/vector/begin/
    // TODO: This should be made into a function
    int vi;
    for(vi = 0; vi < V.size(); vi=vi+2) {
        uint64_t begin = V[vi];
        uint64_t end = V[vi+1];
        for(std::vector<uint64_t>::iterator it = overlap.begin(); it != overlap.end(); it++) {
            uint64_t check = *it;
            if(check == 0) continue;
            if(check > begin && check < end) {
                printf("Jump in same code block 0x0%x\n", begin);
            }
        }
        printf("0x0%x : 0x0%x\n", begin, end);
    }
/*

*/
    // iterate over decompiled blocks to see  pseudo code
    /*
    if(block_a_offset < block_n_end && block_a_offset > block_n_start) {
        printf("Possible overlapping code block at block_a_offset");
    } 

    */
    cs_free(cs_instructions, 1);
    cs_close(&disassembly);

    return 0;
}

int linear_disassemble(Binary *bin, const char *_section) {
/*
    typedef struct cs_insn {
        unsigned int    id; //unique in architecture, ie: X86_INS_CALL 
        unint64_t       address;
        unint16_t       SIZE;
        unint8_t        bytes[16];
        char            mnemonic[32]; //instruction string
        char            op_str[160]; //instruction string operands
        cs_detail       *detail;
    } cs_insn
*/
    csh disassembly;
    cs_insn *insn;
    Section *section;
    size_t count;

    // Get metadata on binary from the loader to ease passing the right arguments to capstone functions:
    uint32_t bits = bin->bits;
    std::string arch_str = bin->arch_str;
    int arch = bin->arch; //To add more architecture support I need to add more to the Binary::BinaryArch enum types.

    section = bin->get_section(_section); //TODO: Change this later when we replace with generic
    if(!section) {
        printf("[ !get_section() ]: Failed to load section\n");
        return 1;
    }

    if(bits == 64 && arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_64, &disassembly) != CS_ERR_OK) {
            fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
            return -1;
        }
    }
    else if(bits == 32 &&arch == 1) {
        if(cs_open(CS_ARCH_X86, CS_MODE_32, &disassembly) != CS_ERR_OK) {
            fprintf(stderr, "[ !cs_open() ]: Failed in capstone function cs_open()\n");
            return -1;
        }
    }

    // args[3-5] come from Class Section from binary loader.
    count = cs_disasm(disassembly, section->bytes, section->size, section->vma, 0, &insn);
    if(count > 0) { //if we actually have bytes.
        size_t j;
        for(j = 0; j < count; j++) {
            printf("0x%" PRIx64 ":\t%s\t\t%s\n", insn[j].address, insn[j].mnemonic, insn[j].op_str);
        }

        cs_free(insn, count);

    }
    else {
        printf("[ !disassemble() ]: Failed to disassemble section\n");
        fprintf(stderr, "  %s\n", cs_strerror(cs_errno(disassembly)));
    }
    cs_close(&disassembly);

    return 0;
}


