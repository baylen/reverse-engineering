#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <capstone/capstone.h>
#include <map>
#include <assert.h>
#include "../include/loader.h"
#include "../include/disassembler.h"
#include "../include/gadgets.h"

// TODO add command to list function list
// TODO add command to disassembly a function or from an address ie: linear f_0x00000020 or linear 0x00000020
int main(int argc, char **argv) {
    /* C
    * Interactive commands
    */

    const char *linear = "linear";
    const char *recursive = "recursive";
    const char *gadgets = "gadgets";

    const char *help = "help";
    const char *quit = "quit";

    /*
    * Load the binary, set up control flow variables for interactive commands
    */
    Binary bin;
    std::string fname;

    if(argc < 2) {
        printf("Usage: %s <binary>\n", argv[0]);
        return 1;
    }

    fname.assign(argv[1]);
    if(load_binary(fname, &bin, Binary::BIN_TYPE_AUTO) < 0) {
        printf("[ !load_binary() ]: Failed to load binary at call\n");
        return 1;
    }

    /*
    * Enter the command loop
    */
    while(1) {
        char input[100];
        printf(": ");
        // https://www.mediafire.com/file/rp14oqf20ixd070/A_beginners%2527_guide_away_from_scanf%2528%2529_%25287_12_2022_2_40_51_PM%2529.html/file
        if(fgets(input, 100, stdin)) {
            input[strcspn(input, "\n")] = 0;
        }

        int ret;
        if(strcmp(input, linear) == 0) {
            // linear
            int retval;
            retval = linear_disassemble(&bin, ".text");
            if(retval != 0) {
                printf("Unable to disassemble %s\n", fname);
                return 1;
            }
            continue;
        }
        if(strcmp(input, recursive) == 0) {
            int retval;
            retval = recursive_disassemble(&bin, ".text");
            if(retval != 0) {
                printf("Unable to disassemble %s\n", fname);
                return 1;
            }
            continue;
        }
        if(strcmp(input, gadgets) == 0) {
            int retval;
            retval = find_rop_gadget(&bin, ".text");
            if(retval != 0) {
                printf("Found no possible ROP gadgets!\n");
            }
            continue;
        }
        if(strcmp(input, help) == 0) {
            printf(" linear:\tperform static disassembly (good for small code blocks)\n");
            printf(" recursive:\tperform recursive disassembly\n");
            printf(" gadgets:\tSearch for gadgets\n");
            printf(" help:\tPrint this help menu\n");
            printf(" quit:\t** Quit **\n");
            continue;
        }
        if(strcmp(input, quit) == 0) {
            printf("Exiting program!\n");
            break;
        }
        else {
            printf("Invalid Command: Enter \"help\"\n");
        }
    }
    /*
    * Print the enumerated values from loader's Binary class:
        std::string          filename;
        BinaryType           type;
        std::string          type_str;
        BinaryArch           arch;
        std::string          arch_str;
        unsigned             bits;
        uint64_t             entry;
        std::vector<Section> sections;
        std::vector<Symbol>  symbols;
    */
   // TODO: Add here more metadata about the program and analysis results
    printf("============ Program Data Summary ============\n");
    /*
    if(dis_retval == 0) {
        printf("  Linear Disassemble: [OK]\n");
    } else { 
        printf("  Linear Disassemble: [Failed]\n");
    }
    if(rdis_retval == 0) {
        printf("  Recursive Disassemble section: [OK]\n");
    } else {
        printf("  Recursive Disassemble: [Failed]\n");
    }
    */
    printf("  Name: %s\n", bin.filename.c_str());
    printf("  Bits: %d\n", bin.bits);
    printf("  Architecture: %s\n", bin.arch_str.c_str());
    printf("  Entry Point: 0x%x\n", bin.entry);
    // Unload binary and exit program
    unload_binary(&bin);
    return 0;
}
