/* Demonstrate the binary loader from ../inc/loader.cc */

#include <stdio.h>
#include <stdint.h>
#include <string>
#include <cstring>

#include "../include/loader.h"


int main(int argc, char *argv[]) {
    size_t i;
    Binary bin;
    Section *sec;
    Symbol *sym;
    std::string fname, dumpsec;
    bool def = false;

    if(argc < 2) {
        printf("Usage: %s <binary> optional: [ section ]\n", argv[0]);
        return 1;
    }

    fname.assign(argv[1]);
    if(argv[2]) {
    // add better handling here
        def = true;
        dumpsec.assign(argv[2]);
    }

    if(load_binary(fname, &bin, Binary::BIN_TYPE_AUTO) < 0) {
        return 1;
    }

    printf("loaded binary '%s' %s/%s (%u bits) entry@0x%016jx\n", 
        bin.filename.c_str(), 
        bin.type_str.c_str(), bin.arch_str.c_str(), 
        bin.bits, bin.entry);

    for(i = 0; i < bin.sections.size(); i++) {
        sec = &bin.sections[i];
        printf("  0x%016jx %-8ju %-20s %s\n", 
             sec->vma, sec->size, sec->name.c_str(), 
             sec->type == Section::SEC_TYPE_CODE ? "CODE" : "DATA");
    }
    //TODO: These for loops should be put into some sort of switch statement of something so they can be called manually from the command line
    if(bin.symbols.size() > 0) {
        printf("scanned symbol tables\n");
        // Print FUNCs (functions)
        for(i = 0; i < bin.symbols.size(); i++) {
            sym = &bin.symbols[i];
            printf("  %-40s 0x%016jx %s\n", 
                sym->name.c_str(), sym->addr, 
                (sym->type & Symbol::SYM_TYPE_FUNC) ? "FUNC" : "");
        }
        // Print GLOBALs
        for(i = 0; i < bin.symbols.size(); i++) {
            sym = &bin.symbols[i];
            printf("  %-40s 0x%016jx %s\n", 
                sym->name.c_str(), sym->addr, 
                (sym->type & Symbol::SYM_TYPE_GLOBAL) ? "GLOBAL" : "");
        }
        // Print LOCALs
        for(i = 0; i < bin.symbols.size(); i++) {
            sym = &bin.symbols[i];
            printf("  %-40s 0x%016jx %s\n", 
                sym->name.c_str(), sym->addr, 
                (sym->type & Symbol::SYM_TYPE_LOCAL) ? "LOCAL" : "");
        }

        /*TODO
        Expand the binary loader so that if a weak symbol is later overridden 
        by another symbol, only the latest version is kept. Take a look at
        /usr/include/bfd.h
        */
        // Print WEAK Symbols
        for(i = 0; i < bin.symbols.size(); i++) {
            sym = &bin.symbols[i];
            printf("  %-40s 0x%016jx %s\n", 
                sym->name.c_str(), sym->addr, 
                (sym->type & Symbol::SYM_TYPE_WEAK) ? "WEAK" : "");
        }

    }

    // TODO: Need to fix output format of of section dumps so it is actually readable.
    if(def == true) {
        // Dump section contents
        bool found = false;
        for(i = 0; i < bin.sections.size(); i++) {
            sec = &bin.sections[i];
            if(strcmp(sec->name.c_str(), dumpsec.c_str()) == 0) {
                found = true;
                int sec_size = sec->size;
                int j;
                unsigned char buff[sec_size + 1];
                uint64_t sec_start = sec->vma;
                printf("\n[DUMPING CONTENTS]\n  Section: %s\n  size: %d\n  Address Start: 0x%08x\n\n", dumpsec.c_str(), sec_size, sec_start);
                for(j = 0; j < sec_size + 1; j++) {
                    buff[j] = sec->bytes[j];
                    printf("0x%02x ", (unsigned)buff[j]);
                }
            }
        }
        if(found == false) {
            printf("WARNING: No section named '%s'!\n", dumpsec.c_str());
        }
    }

    unload_binary(&bin);

    return 0;
  }

