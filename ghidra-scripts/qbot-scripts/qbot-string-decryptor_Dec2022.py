#!/usr/bin/python3
import sys
import argparse
import os

'''
This script takes a key and the ciphertext of the strings that are used. As default the usage takes three arguments: the ciphertext, the key, and an offset in base16.

Example: qbot-string-decryptor.py ciphertext_strings.bin keyblob.bin 0x0

The above will decrypt the first string. If you have discovered a bunch of offsets that are called then you can comment out the offset variable and add them all to a list in the "offsets" array variable instead. This could be integrated into a more complete script to automate reversee engineering tasks.

'''

'''Get the offsets by searching for NULL terminators in the decrypted
   ciphertext and return a list of offsets where strings start
'''
def get_offsets(keyBlob, moduleCipherText, size=None):
    if size == None:
        size = 0x1169
    offsets = []

    i = 0
    while i < size:
        pt_byte = keyBlob[i % 128] ^ moduleCipherText[i]
        # If the decrypted byte is a terminator then break

        if pt_byte == 0x0:
        # Here we append the offset
            offsets.append(i + 1)
        i = i + 1
    # Add 0x0 value to start of index list because the first string starts here.
    offsets.insert(0, 0)
    return offsets


def decrypt_modules(offsets, keyBlob, moduleCipherText, size=None):
    if size == None:
        size = 0x1169

    for offset in offsets:
        pt_byteArray = bytearray(size)

        i = 0
        while i < size:
            # Decrypt a byte and assign it to pt_byte, then append it to our pt_byteArray
            pt_byte = keyBlob[(offset + i) % 128] ^ moduleCipherText[offset + i]
            pt_byteArray.append(pt_byte)

            # If the decrypted byte is a terminator then break
            if pt_byte == 0x0:
                break

            i = i + 1
        # Print both to console
        pt_string = pt_byteArray.decode('utf8')
        print(hex(offset) + ": " + pt_string)
        #print(hex(offset) + ": " + str(pt_byteArray))


def main():
    try:
        mct = open(sys.argv[1], "rb").read()
    except:
        print("Failed to open module cipher text binary data")
        return 1
    try:
        kb = open(sys.argv[2], "rb").read()
    except:
        print("Failed to open keyblob binary data")
        return 1

    #add the offsets here and remove the comment and size in the decrypt_modules function if you only want to decrypt a specific string. The default will usually decrypt the entire blob before failing.
    #offset = sys.argv[3]
    #offset = int(offset, base=16)
    #offsets = [offset]

    #decrypt_modules(offsets, kb, mct, 0x20)
    indexes = get_offsets(kb, mct)


    decrypt_modules(indexes, kb, mct)
    #print(indexes)

    try:
        mct.close()
    except:
        print("Failed to close handle. Possible byte object")
    try:
        kb.close()
    except:
        print("Failed to close handle. Possible byte object")

if __name__ == '__main__':
    main()
