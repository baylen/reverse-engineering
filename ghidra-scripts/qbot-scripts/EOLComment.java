//TODO write a description for this script
//@author 
//@category Tests
//@keybinding 
//@menupath 
//@toolbar 

import ghidra.app.script.GhidraScript;
import ghidra.program.model.mem.*;
import ghidra.program.model.lang.*;
import ghidra.program.model.pcode.*;
import ghidra.program.model.util.*;
import ghidra.program.model.reloc.*;
import ghidra.program.model.data.*;
import ghidra.program.model.block.*;
import ghidra.program.model.symbol.*;
import ghidra.program.model.scalar.*;
import ghidra.program.model.listing.*;
import ghidra.program.model.address.*;


import java.util.*;

import ghidra.app.script.GhidraScript;
import ghidra.program.model.address.Address;
import ghidra.program.model.listing.CodeUnit;
import ghidra.program.model.listing.Listing;


public class EOLbyAddress extends GhidraScript {

    @Override
    public void run() throws Exception {

        // input address
        Address address = askAddress("Enter address", "Enter the address to add an end-of-line comment:");

        // check the address
        if (!currentProgram.getMemory().contains(address)) {
            println("Invalid address: " + address.toString());
            return;
        }

        // input comment
        String comment = askString("Enter comment", "Enter the end-of-line comment:");

        // check the comment
        if (comment == null || comment.trim().isEmpty()) {
            println("Invalid comment: " + comment);
            return;
        }

        // Get the listing
        Listing listing = currentProgram.getListing();

        // Get to the address
        CodeUnit codeUnit = listing.getCodeUnitAt(address);

        // Add the comment
        codeUnit.setComment(CodeUnit.EOL_COMMENT, comment);

        // logging 
        println("End-of-line comment added successfully at address " + address.toString());

    }

}
