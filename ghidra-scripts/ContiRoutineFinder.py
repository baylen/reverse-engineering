# Script to rename decryption subroutines found in a release binary of the conti version 3 ransomware that was leaked by some dude on twitter
#@author Baylen Orr
#@category Functions
#@keybinding 
#@menupath 
#@toolbar 

from ghidra.program.model.block import BasicBlockModel
from ghidra.util.task import ConsoleTaskMonitor


state = getState()
cp = state.getCurrentProgram()
name = cp.getName()
exeLoc = cp.getExecutablePath()
listing = cp.getListing()
blockModel = BasicBlockModel(cp)
monitor = ConsoleTaskMonitor()

print(exeLoc)
print(name)
print(state)

def renameRoutine(entryAddress, listing):
   # does function at entryAddress start with "FUN"
   func = getFunctionAt(entryAddress)
   currentName = func.getName()
   if currentName.startswith("FUN"):
      funcObject = listing.getFunctionAt(entryAddress)
      print("[renameRoutine] renaming function to decryptRoutine_{}".format(entryAddress))
      newName = "decryptRoutin_{}".format(entryAddress)
      funcObject.setName(newName, ghidra.program.model.symbol.SourceType.DEFAULT)
   else:
      print("[renameRoutine] {} already named".format(entryAddress))
      
      
def checkInstructions(entryAddress):
   # Get offset to decrypt routine and do a sanity check on first instruction
   entry = entryAddress
   decryptLabel = entry.add(21)
   labelEntry = getInstructionAt(decryptLabel)

   expected = ['MOV','LEA','MOVZX','SUB','IMUL','CDQ ','IDIV','LEA ','CDQ','IDIV','SUB','JNZ','POP', 'SHL']
   i = 0
   while i < 33:
      instruction = listing.getInstructionAt(decryptLabel)
      
      try:
         tmp = instruction.getMnemonicString()
      except:
         tmp = False

      if tmp not in expected:
         print("{} Possible mismatch. Check manually against {}".format(entry, tmp))
         return False
      else:
         return True
      decryptLabel = decryptLabel.add(1)
      i += 1


func = getFirstFunction()
while func is not None:
   # Get the size of the function
   min = func.getBody().getMinAddress()
   max = func.getBody().getMaxAddress()
   funcSize = max.subtract(min) + 1

   # If function size is 58:
   if funcSize > 58 and funcSize < 100:
       # enumerate instructions in function
       entry = func.getEntryPoint()
       found = checkInstructions(entry)

       if found == True:
          # attempt to rename function
          try:
             renameRoutine(entry, listing) 
          except:
             pass
   func = getFunctionAfter(func)
