## Contributing to yara rules:
This is a list of yara rules from worked cases. Due to naming conflicts in Resilient between the REM and GSFC instances, name the rule after the REM ID field.

Example:
> 12345.yar


Include the following `meta` tag:
> Note: The tool `pehash` on remnux will calculate all except tlsh. The fuzzy hash algorithm ssdeep is in the process of being replaced by tlsh in industry, so try and include tlsh as well. The tool has to be built (https://github.com/trendmicro/tlsh). The tlsh algorithm has also been adopted by VirusTotal, and can be retrieved there.
1. description = "A brief description of the rule"
2. date    = "DD-MM-YYYY"
3. author  = "Author Name or Alias"
4. sha256  = " _hash_ "
5. ssdeep  = " _hash_ "
6. tlsh    = " _hash_ "
7. resilient = "####"
