rule lokibot_loader {
	meta:
		description = "Malicious DLL from case 39455"
		author = "Baylen Orr"
        hash = "6a03dd032b8dba7ac5d4a2fdb65b0108bab37102f21c761e1b4df1ce8c46782c"
        ssdeep = "6144:bjaRHw8BiML/Bz4UuqAeaMjxk5Bf9C9NRd2CiJCDVnTgY2Vb+M6g03bvRFMDOUX:QBDrBzNlpjxkHFC9l2PJw26g03DDVa2"
        tlsh = "T16274286585D3003FDA91E5F1E5895586C3EB2D0C79CADFEAE91E083EBC5012192BEE0D"
        imphash = "f6fd955594f76963cb9fbacdb1240455"
        resilient = "39455"
	strings:
		$s0 = "aghjbien.dll"
		$s1 = "anxxjw"
		$s2 = "emvpva"
		$s3 = "frzerxvgw"
		$s4 = "jofiqktddqk"
		$s5 = "miaxnjzzg"
		$s6 = "nbvtnq"
		$s7 = "nmooq"
		$s8 = "ochahe"
		$s9 = "ocjawtbsvl"
		$sa = "tsrlgihacep"
		$sb = "ufayy"
		$sc = "vjktxhz"
		$sd = "xiqoecpi"
		$se = "ychxtstvft"
		$sf = "zwovgdrahfq"
	condition:
		5 of them
}

rule lokibot_payload {
	meta:
		description = "Stage one executable"
		author = "Baylen Orr"
		sha256 = "888639b1a4075ec38c227db0de40aae81b87953144230b3343f962378905ddc1"
		ssdeep = "1536:9zvQSZpGS4/31A6mQgL2eYCGDwRcMkVQd8YhY0/EqfIzmd:mSHIG6mQwGmfOQd8YhY0/EqUG"
		tlsh = "T18FA32A42B2A5C030F7B74DB2BB73A5B7857E7C332D22C44E9352859A14215E1EB7AB13"
		imphash = "0239fd611af3d0e9b0c46c5837c80e09"
        resilient = "39455"
    strings:
		$s0 = "U2XpekVvtYq0fwsx7EDuZjrCo9GcF1B6Hl358mbznyLWdMANa4TSKJhIiOPgQR"
		$s1 = "DlRycq1tP2vSeaogj5bEUFzQiHT9dmKCn6uf7xsOY0hpwr43VINX8JGBAkLMZW"
		$s2 = { 97 8b 8b 8f c5 d0 d0 8c 9a 9c 8a 8d 9a cf ce d2 8d 9a 9b 96 8d 9a 9c 8b d1 91 9a 8b d0 98 9c cb d0 99 8d 9a d1 8f 97 8f 00 }

	condition:
		any of them and (filesize > 200KB)
}
