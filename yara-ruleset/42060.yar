rule po_pou {
    meta:
        description = "A .NET exectabule retrieved from the Malware Encrishment Portal flagged with Trojan.Multi.GenericML. See REM resilient case 42060 for further details"
        author = "Baylen Orr"
        date = "20April2022"
        sha256 = "5513b8e5c022575752d7e4f018e7f7a0a479475351fb56ec76e002a0b58b041c"
        ssdeep = "192:rOe/I4Sgq/VI4w2zGitmSyAFZzScHnlnffffffxKDJEPKDaYHq8kb9v:6RdxGem7e+2BffffffxKD+PKDaYH2b9"
        imphash = "f34d5f2d4577ed6d9ceec516c1f5a744"
        tlsh = "T1B072097113186477CAE8493B48D756500332FA078062FB3E6DDC853FAE23B65D7A532A"
        date = "20-04-2022"
    strings:
        $s0 = "pou poooo"
        $s1 = "pou poooo.exe"
        $s2 = "m_Pool"
        $s3 = "Mcjwfr.Properties.Resources.resources"
        $s4 = "https://discord.com/channels/@me/956150575911813140/965743467131187221"
        $s5 = {4E 00 65 00 76 00 73 00 6D 00 62 00 6B 00 66 00 7A 00 74 00 79 00}
    condition:
        2 of them and filesize > 15KB
}
