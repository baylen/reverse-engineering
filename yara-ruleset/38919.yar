rule excel_macro_domains {
	meta:
		description = "HTML requests from an html request from a malicious executable that was dropped onto a system with Excel macros."
		author = "Baylen Orr"
		date = "2021-12-21"
        resilient = "38919"
	strings:
		$s1 = "https://notyouraveragecookie.com/uploads/6573/g48kJ8wad.dll"
		$s3 = "dr49lng3n1n2s.cloudfront.net"
		$s4 = "https://linxpaperlimited.com//wp-content/uploads/2021/12/12/#13v50qca7Cs0s-dA96qEtYgEPz_qhnLqm"
		$s5 = "https://awaywardlife.com/wp-content/uploads/2021/12/12/#11SkM0UZFNix032wqjIs_61iP0Pv_YVy-"
	condition:
		$s1 or $s3 or $s4 or $s5
}

rule loki_retrieved_payload_domain {
    meta:
        description = "Dropped DLL attempted to retrieve information from a hardcoded URL.
        author = "Baylen Orr"
        date = "2021-12-21"
        tlsh = "T1C155D587EA6361E0E4BBD23486A6772BB97135148334C7CB87454B174B22FF4A97E384"
        ssdeep = "24576:ZD0Ejqw95oEKe3UyVlViimjKaH1S2uK7SPFL3EOGTWqG5QVEzAJ24GOy2i5b8+D:d0be3UyVlVFmjKaH1S237SPFL3EOGTW"
        resilient = "38919"
    strings:
		$s2 = "http://endofyour.ink"
    condition:
        $s2
}
