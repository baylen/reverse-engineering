rule agentTesla_xena
{
    meta:
        author = "Baylen Orr"
        date = "29 April 2022"
        description = "stage1_TokenAccessLev.exe from Agent Tesla"
        sha256 = "cd8717a51e5b9667a820f310e65669364416914b67aa9cc1fda7ac1e8deee35a"
        ssdeep = "12288:YcVHHYdr/M6t5aYbFzpQ2zaXW5ecxh3fA946+HEXWPFr:xnWrTt5aYb5pQrm5Fhvc3gcE"
        tlsh = "T156E4CD946F16D44DE6883D77A88194F103E1EE01AC0EF58A74F6374E66F6EE3C6A1342"
        date = "28-04-2022"
        resilient = "42188"
    strings:
        $s0 = "Xena"
        $s1 = "Alleme.Properties"
        $s2 = {13 30 07 00 50 00 00 00 00 00 00 00 38 02 00 00 00 03 2A 03 04 20 00 36 00 00 5D 03 04 20 00 36 00 00 5D 91 02 04 1F 16 5D 28 8B 00 00 06 61 28 6F 00 00 06 03 04 17 58 20 00 36 00 00 5D 91 28 6F 00 00 06 59 20 00 01 00 00 58 20 00 01 00 00 5D 28 8C 00 00 06 9C 38 B5 FF FF FF}
    condition:
        all of them

}

rule agentTesla_xena_trojan
{
    meta:
        author = "Baylen Orr"
        date = "29 April 2022"
        description = "stage2_abzQueueReader.dll from Agent Tesla"
        sha256 = "af1f5074eff36653e6d54c5c7aabf990aee2cc70a72db4cfb1707e452349c574"
        ssdeep = "192:iz4AvXz4UatagvDdcSAWHECWtecJKzXTs1r:WpL4ogpc4ECWtHYk5"
        tlsh = "T17952094C8BD8A23EDE9E47BAE8F62A59437061122B13D31F4AC46065BC73374D53365E"
        date = "28-04-2022"
        resilient = "42188"
    strings:
        $s0 = "Forging"
        $s1 = "Forging.ParsingState"
        $s3 = "GlobalAssemblyCache"
    condition:
        all of them

}

rule agentTesla_stage3
{
    meta:
        author = "Baylen Orr"
        date = "29 April 2022"
        description = "stage3_DotNetZipAdditionalPlatforms.dll from Agent Tesla"
        sha256 = "91c0e6ab4dbb88f6598a8dc6496da665c2b76cc3b2de773c6347adbfa792ee9a"
        ssdeep = "6144:nGmUPTzVEp8Bs53kGo31PZKFbg17oJdytF21TlKh4iNy68juNLO8zbuXDUc0LUO:nGt7TXfOFuilK/y68X8zXFjA+lNmTH5"
        tlsh = "T181B4AF01A6EADF12C76402BBC4D31F5493FAD45B96B2DB5B216422DD2C07397E90278F"
        date = "28-04-2022"
        resilient = "42188"
     strings:
        $s0 = {49 56 77 30 34 71 47 66 4A 69 5A 57 4F 4E 66 6D 68 45}
    condition:
        $s0
}

rule agentTesla_stage1_overwrite_and_execute
{
    meta:
        author = "Baylen Orr"
        date = "29 April 2022"
        description = "ACBBUrbKpBNFcXasbucMjNhXYxjTx.exe from Agent Tesla"
        sha256 = "025fd34b016ccc03c7b25390c3c0478943d182fed1198311d62383475d055753"
        ssdeep = "3072:4Zt5zr0yqMzpVH2HwUFE6bLDL6TlzhUao4KZlMuCJrYoSTCw4UTRi3nQ2TjQIkk:DbnaHUUklINGM9jQIk1taU49"
        date = "28-04-2022"
        tlsh = "T174242A1A6715D551CB5F417FC01AC21831F1EB03A72AE78F19A2D8F91B821CEF92B8E5"
        resilient = "42188"
   strings:
        $s1 = "ACBBUrbKpBNFcXasbucMjNhXYxjTx.exe"
        $s2 = "SetAttributes"
        $s3 = "Rfc2898DeriveBytes"
        $s4 = "ReadAllBytes"
        $s5 = "GetAddressBytes"
        $s6 = "GetBytes"
        $s7 = "get_Values"
        $s8 = "SocketFlags"
        $s9 = "Strings"
        $sa = "SocketAsyncEventArgs"
        $sb = "ElapsedEventArgs"
        $sc = "ResolveEventArgs"
        $sd = "get_Ticks"
        $se = "ICredentials"
        $sf = "get_Credentials"
        $sg = "set_Credentials"
        $sh = "get_DefaultCredentials"
        $si= "cset_UseDefaultCredentials"
   condition:
        $sf or $sg or $sh or $si and (4 of them)
}
