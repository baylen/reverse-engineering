rule conti_v3_parameterExpansion {
	meta:
		description = "MetaBuffer expansion routine to decrypt strings. This routine follows a stack string sequence."
		author = "Baylen Orr"
		sha256 = "1765c1f89ce0184c0ce0bb634ef4853afe10c080f954ca22ab2f7a58de434b51"
		ssdeep = "3072:s5+bn2KwlCnJWeLrmmdaPt82oqY0nDF6yGSMdUVwJCSlFN70zx:s5gn2KjWuHoaf8D0ypMKg0zx"
		tlsh = "T14A243961F5885039E154187619BC7EE254B9AE38331FC8F333DD89BE9A646C2B124F4B"
	    date = "29-03-2022"
        resilient = "41254"
    strings:
		$s0 = {8a 44 ?? ?? 0f b6 ?? 83 ?? ?? 6b ?? ?? 99 f7 ?? 8d ?? ?? 99 f7 ?? 88 ?? ?? ?? ?? 83 ?? ?? 72 ??}
	condition:
		any of them
}


rule array_decryptor_routine {
	meta:
		description = "Routine decrypts stack strings into a single variable which is appended to a 170 byte array"
		author = "Baylen Orr"
		sha256 = "1765c1f89ce0184c0ce0bb634ef4853afe10c080f954ca22ab2f7a58de434b51"
		ssdeep = "3072:s5+bn2KwlCnJWeLrmmdaPt82oqY0nDF6yGSMdUVwJCSlFN70zx:s5gn2KjWuHoaf8D0ypMKg0zx"
		tlsh = "T14A243961F5885039E154187619BC7EE254B9AE38331FC8F333DD89BE9A646C2B124F4B"
	    date = "29-03-2022"
        resilient = "41254"
    strings:
		$s0 = {8a ?? 8d ?? ?? ?? ?? ?? 83 ?? ?? 6b ?? ?? ?? f7 ?? 8d ?? ?? ?? f7 ?? 88 ?? ?? 83 ?? ?? 75}
	condition:
		any of them
}
