rule seven533_dll_quakbot_chain {
    meta:
        description = "A dll file part of a Qbot infection chain. See rpt-44098 for more details"
        author = "Baylen Orr"
        date = "18-07-2022"
        sha256 = "0a0a0dd26e928d48f841782cd8180a0eaffed41d5593a53a564ff3706982a9f4"
        ssdeep = "12288:Fm7UnRfVgZK5uH5yYTKGhkJ9JWUjH5W5CFz0pKuQnjhVh7SNE:FmwZGZJAY17UlOiuQlVJ"
        tlsh = "T14AE49E32F2F14433D1761A7D8D7F9668A8297F412D38994A2FE61E4C4F396813A27387"
        resilient = "44098"
    strings:
        $s0 = "tttygttttttttttttttttttttt"
        $s1 = {68 e1 fa ed 0e ff 15 14 e0 46 00}
        $s2 = {68 e0 fa ed 0e ff 15 14 e0 46 00}
        $s3 = {68 e3 fa ed 0e ff 15 14 e0 46 00}
        $s4 = {68 de fa ed 0e 52 ff 25 14 e0 46 00}
    condition:
        2 of them
}
