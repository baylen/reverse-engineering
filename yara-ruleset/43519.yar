rule moonlight_WpluginDLL {
    meta:
        description = "Dropped DLL to \\AppData\\Roaming\\Wplugin.dll. Called with GetProcAddress(Wplugin.dll,HandleCode)"
        date = "20-06-2022"
        author = "Baylen Orr"
		sha256 = "2e9203956467c3cbdbbaf88b6450f9dece0cf3f3099fff5f32ecf5edbb5eaa90"
		tlsh = "T199B36E11F6028036C0D350BD5626A72BE7397A7126609D87E7E0AF793E751E1AE313CB"
        ssdeep = "1536:lFDUIYkK4cEui1gNNx0eW6QPB/4ZW1ck8zHhTduu+qjuk9WkD2ofx9t:7hYZ4z1sxtbjuUWnoZ9"
        resilient = "43519"
    strings:
        $s0 = "UploadFile1.asp"
        $s1 = {25 6c 64 3a 50 4f 53 54 3a 74 65 73 74 65 00}
        $s2 = {25 73 3a 25 73 3a 25 73 3a 25 73 3a 61 75 74 68 3a 25 73 00}
        $s3 = "Urgent"
        $s4 = "Something"
        $s5 = "File to download"
        $s6 = {25 6c 64 3a 47 45 54 3a 74 65 73 74 65 00}
        $s9 = "Remenber"
        $sa = "Course"
        $sb = "Request accepted"
        $sc = "New work"
        $sd = "Registration"
        $se = "SENDED:"
        $sf = "Yes"
        $sg = {48 45 4c 4f 20 3c 25 73 3e 00}
        $sh = "UploadFile2.asp"
        $si = {20 3c 61 20 68 72 65 66 3d 22 24 25 73 3f 25 73 22 3e 25 73 3c 2f 61 3e 3c 62 72 3e 3c 62 72 3e 0a 00}
    condition:
        4 of them
}

