rule fake_microsoftteamsdll {
	meta:
		description = "Rules for file `Microsoft.Teams.Diagnostics.dll` from R-39554"
		author = "Baylen Orr"
		sha256 = "9c6ab79665beba11c0b460488c1bebad7369edd5ed63874e9e8e4a255199652f"
		ssdeep = "384:oiZth/ob/VCQa8L21I5qaI2b2rgz2LsjzA7TIrqXwhWYkQWzOImlhQdKLHRN7ESX:Vth/Cz6IpXbroWykrqgcPAGg"
        date = "14-01-2022"
        resilient = "39554"
	strings:
		$s0 = "H4sIAAAAAAAEAI2RTUvDQBCG7/6KZc/p5oNYaUkEtZ600EPwPlkn6dLsbNiPYP69FWKkNYfOcYb34WHeQhpqVBsseGXo8Y5NU9hAXmn828wXcA513Y3Pij4VtexLd+RKHixtnTyiBrfSSlrjTONX0ugtOC2GlP9HzcjemvoH1Vs1gMcD+GPJe5AnaNHFPF6wiK80lkT7fmc0KNoDnUH2aUqwAbqAJd//WooKQTuxU9CScV5JF7EPtO78kTIVicjSLNmILGIvofPBYkkYvIUuYodQd0q+4ViZE1IJMr/fyCTP12mTPuCasyX1a69q7PEGJ/E6IPl3006xC3gRz4UV8WWn3yL2gzzjAQAA"
		$s1 = "https://cdn.hendersonandjenkinsfinancial.com/fonts/PNr5TRASf6M7Q.woff2"
		$s2 = "H4sIAAAAAAAEAPPNr8rMyUnUN9UzUNAIz8xLyS8vVvALUTA00DOwVgAKmJlYK1SYmWgqOBYU5KSGpyZ5Z5bomxqb6xmbKWh4e4T4+ugo5GRmpyq4pyZn52sqOGcU5eem6lua6RnomZiZmeiZmCoEJ6YlFmXCdLmmpEOkDQ1MTfSMLAF/mlurgwAAAA=="
		$s3 = "H4sIAAAAAAAEAMsoKSkottLXT07J08tIzUtJLSrOz0vMS8lKzcvOzCtOywRykjMTc/SS83P1c/LTM4HKSnJzAIQwudk3AAAA"
		$s4 = "{0} {1} {2} {3} {4} {5}"
	condition:
		$s4 and $s0 or $s1 or $s2 or $s3
}
