rule imports_dridex {
    meta:
        description = "Invalid checksum in Rich Header"
        author = "Baylen Orr"
        date = "16-05-2022"
        sha256 = "a16fce2955ac93ba1e3b7fff5755b943601260d5574d1165e8592f3a2caf109a"
        ssdeep = "3072:Ir6BOtyO+T2oGCQF2KBjSwCnqD0DIoYDlssdKHny2hoe9tr0T:IrQ+yRT2oGCQF2KBOwCn9DIJDlssdW5"
        imphash = "eb76140cc31cd615e218e51dbcad0a7b"
        tlsh = "T168F37D23AC0181B9F9B434B4A6AC7B6D54AD883B031B10D7C7F66CE81F155E36B39276"
        resilient = "42248"
    strings:
        $s0 = "FreeConsole()"
        $s2 = "ExitProcess()"
        $s3 = "GetComputerNameW()"
        $s4 = "AddVectoredExceptionHandler()"
        $s5 = "OutputDebugString()"
        $s6 = "Sleep()"
        $s7 = "GetUserNameW()"
        $s8 = {7E 5B 5E B5 3A 3A 30 E6 3A 3A 30 E6 3A 3A 30 E6 33 42 B3 E6 3B 3A 30 E6 33 42 A3 E6 3D 3A 30 E6 3A 3A 31 E6 36 3A 30 E6 3A 3A 30 E6 3B 3A 30 E6 21 A7 9B E6 1A 3A 30 E6 21 A7 AB E6 3B 3A 30 E6 21 A7 AD E6 3B 3A 30 E6 52 69 63 68 3A 3A 30 E6}
    condition:
        $s8 and (4 of ($s0,$s2,$s3,$s4,$s5,$s6,$s7))
}

rule running_process_strings {
    meta:
        description = "Strings present in the process memory dump, decoded at runtime"
        author = "Baylen Orr"
        date = "16-05-2022"
        sha256 = "a16fce2955ac93ba1e3b7fff5755b943601260d5574d1165e8592f3a2caf109a"
        ssdeep = "3072:Ir6BOtyO+T2oGCQF2KBjSwCnqD0DIoYDlssdKHny2hoe9tr0T:IrQ+yRT2oGCQF2KBOwCn9DIJDlssdW5"
        imphash = "eb76140cc31cd615e218e51dbcad0a7b"
        tlsh = "T168F37D23AC0181B9F9B434B4A6AC7B6D54AD883B031B10D7C7F66CE81F155E36B3927
        resilient = "42248"
    strings:
       $s1 = "Malware called ResumeThread"
       $s2 = "ollydbg.exe"
       $s3 = "ida.exe"
       $s4 = "ida64.exe"
       $s5 = "idag.exe"
       $s6 = "idag64.exe"
       $s7 = "idaw.exe"
       $s8 = "idaw64.exe"
       $s9 = "idaq.exe"
       $s10 = "idaq64.exe"
       $s11 = "idau.exe"
       $s12 = "idau64.exe"
       $s13 = "scylla.exe"
       $s14 = "scylla_x64.exe"
       $s15 = "scylla_x86.exe"
       $s16 = "protection_id.exe"
       $s17 = "x64dbg.exe"
       $s18 = "x32dbg.exe"
       $s19 = "windbg.exe"
       $s20 = "reshacker.exe"
       $s21 = "ImportREC.exe"
       $s22 = "IMMUNITYDEBUGGER.EXE"
       $s23 = "devenv.exe "
       $s24 = "OLLYDBG"
       $s25 = "disassembly"
       $s26 = "scylla"
       $s27 = "Immunity"
       $s28 = "WinDbg"
       $s29 = "x32dbg"
       $s30 = "x64dbg"
       $s31 = "Import reconstructor"
       $s32 = "Zeta Debugger"
       $s33 = "Rock Debugger"
       $s34 = "ObsidianGUI"
       $s35 = "WinDbgFrameClass"
       $s36 = "idawindow"
       $s37 = "tnavbox"
       $s38 = "idaview"
       $s39 = "tgrzoom"
       $s40 = "explorer.exe"
       $s41 = "Unpacked.exe"
       $s42 = "HookLibraryx86.dll"
       $s43 = "HookDllData"
       $s44 = "HookedGetLocalTime"
       $s45 = "HookedGetSystemTime"
       $s46 = "HookedGetTickCount64"
       $s47 = "HookedGetTickCount"
       $s48 = "HookedKiUserExceptionDispatcher"
       $s49 = "HookedNativeCallInternal"
       $s50 = "HookedNtClose"
       $s51 = "HookedNtContinue"
       $s52 = "HookedNtCreateThread"
       $s53 = "HookedNtCreateThreadEx"
       $s54 = "HookedNtDuplicateObject"
       $s55 = "HookedNtGetContextThread"
       $s56 = "HookedNtQueryInformationProcess"
       $s57 = "HookedNtQueryObject"
       $s58 = "HookedNtQueryPerformanceCounter"
       $s59 = "HookedNtQuerySystemInformation"
       $s60 = "HookedNtQuerySystemTime"
       $s61 = "HookedNtResumeThread"
       $s62 = "HookedNtSetContextThread"
       $s63 = "HookedNtSetDebugFilterState"
       $s64 = "HookedNtSetInformationProcess"
       $s65 = "HookedNtSetInformationThread"
       $s66 = "HookedNtUserBlockInput"
       $s67 = "HookedNtUserBuildHwndList"
       $s68 = "HookedNtUserBuildHwndList_Eight"
       $s69 = "HookedNtUserFindWindowEx"
       $s70 = "HookedNtUserGetForegroundWindow"
       $s71 = "HookedNtUserQueryWindow"
       $s72 = "HookedNtYieldExecution"
       $s73 = "HookedOutputDebugStringA"
    
    condition:
        30 of them
}                                        





























