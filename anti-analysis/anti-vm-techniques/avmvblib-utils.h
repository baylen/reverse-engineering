#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <tlhelp32.h>
#include <string.h>

DWORD walk_process(std::string process) {
	/*
	* Convert input `string process` to a const wchar_t type:
		https://docs.microsoft.com/en-us/cpp/text/how-to-convert-between-various-string-types?redirectedfrom=MSDN&view=msvc-170
	*/
	const size_t new_size = strlen(process.c_str()) + 1; // get new size to get string length
	size_t converted_chars = new_size; // initialize the object with zeros
	wchar_t *wc_string = new wchar_t[new_size]; // get the length of the c string
	mbstowcs_s(&converted_chars, wc_string, new_size, process.c_str(), _TRUNCATE); //converts a multibyte character string from the array whose first element is pointed to by arg 4

	// Now walk the process and find the target
	PROCESSENTRY32 process_entry;
	process_entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (Process32First(snapshot, &process_entry) == TRUE) {
		while (Process32Next(snapshot, &process_entry) == TRUE) {
			printf("in Process32Next\n");
			if (_wcsicmp(process_entry.szExeFile, wc_string) == 0) {
				CloseHandle(snapshot);
				return process_entry.th32ProcessID;
			}
		}
	}

	CloseHandle(snapshot);
	std::cout << "Did not find process: '" << process << "'" << std::endl;
	//printf("Did not find process '%s'\n", process);
	return 0;
}


BOOL walk_threads(DWORD process) {
	printf("Attempting to walk threads of [%d]\n", process);
	// initialize the handle value
	HANDLE target_snapshot = INVALID_HANDLE_VALUE;
	THREADENTRY32 thread_entry32;
	thread_entry32.dwSize = sizeof(THREADENTRY32);
	
	// get a snapshot of all processes:
	target_snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);

	if (Thread32First(target_snapshot, &thread_entry32) == FALSE) {
		CloseHandle(target_snapshot);
		printf("Failed to enumerate threads of PID [%d] at 'Thread32First()'\n", process);
	}
	// enumerate through threads
	while (Thread32Next(target_snapshot, &thread_entry32)) {
		// if the next thread belongs to our target process
		if (thread_entry32.th32OwnerProcessID == process) {
			// print struct members
			std::wcout << "Thread ID: " << thread_entry32.th32ThreadID << std::endl;
			return TRUE;
//			printf("Thread ID: [%d]\n", thread_entry32.th32ThreadID);
//			printf("Thread Kernel Base Priority: [%d]\n", thread_entry32.tpBasePri);
		}
	}
	CloseHandle(target_snapshot);
	return TRUE;
}
/* from microsoft docs:
	A snapshot that includes the module list for a specified process contains information
	about each module, executable file, or dynamic-link library (DLL), used by the
	specified process
*/
BOOL walk_process_modules(DWORD process, std::string target_module) {
	// convert string to WCHAR
	const size_t new_size = strlen(target_module.c_str()) + 1; // get new size to get string length
	size_t converted_chars = 0; // initialize the object with zeros
	wchar_t* wc_string = new wchar_t[new_size]; // get the length of the c string
	mbstowcs_s(&converted_chars, wc_string, new_size, target_module.c_str(), _TRUNCATE); //converts a multibyte character string from the array whose first element is pointed to by arg 4

	
	MODULEENTRY32 module32;
	module32.dwSize = sizeof(MODULEENTRY32);
	HANDLE target_snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, process);

	if (wc_string) {
	std::wcout << "[debug]: target_module = '" << wc_string << "'" << std::endl;
		if (Module32First(target_snapshot, &module32) == FALSE) {
			CloseHandle(target_snapshot);
			printf("Failed to enumerate modules from target snapshot\n");
		}
		while (Module32Next(target_snapshot, &module32)) {
			if (module32.th32ProcessID == process) {
				if (_wcsicmp(module32.szModule, wc_string) == 0) {
					std::cout << "In module32Next" << std::endl;
					/*
					printf("Module Name: %s\n", module32.szModule);
					printf("   Module Path: [%s]\n", module32.szExePath);
					printf("   Module Addr: [0x%08X]\n", module32.modBaseAddr);
					printf("   Module Size: [0x%08X]\n", module32.modBaseAddr);
					*/
				}
			}
		}

		CloseHandle(target_snapshot);
		return TRUE;
	}

	if (Module32First(target_snapshot, &module32) == FALSE) {
		CloseHandle(target_snapshot);
		printf("Failed to enumerate modules from target snapshot\n");
	}

	while (Module32Next(target_snapshot, &module32)) {
		if (module32.th32ProcessID == process) {
			std::cout << "Module Name: ";
			std::wcout << module32.th32ProcessID << std::endl;
			/*
			printf("Module Name: %s\n", module32.szModule);
			printf("   Module Path: [%s]\n", module32.szExePath);
			printf("   Module Addr: [0x%08X]\n", module32.modBaseAddr);
			printf("   Module Size: [0x%08X]\n", module32.modBaseAddr);
			*/
		}
	}

	CloseHandle(target_snapshot);
	return TRUE;
	//printf("[debug]: target_module = '%s'\n", target_module);
	//printf("[debug]: module name = '%s'\n", module32.szModule);
}
