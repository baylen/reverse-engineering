// anti-vm-techniques.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <windows.h>
#include <windef.h>
#include <string>
#include <iostream>
#include <winreg.h>
#include <tlhelp32.h>
#include <tchar.h> //useful for printing information from CreateProcessToolhelp32 function
#include <stdio.h>
#include "avmvblib.h"

using namespace std;
int main()
{
    // initialize some variables for testing functions:
    int is_vm_cpu;
    int mac;
    int disk_check = 0;
    string target_module = "Somemodule.dll";
    string vbox_exe = "VBoxTray.exe";
    char scsi_disk[] = "VBOX    HARDDISK";
    LPCSTR disk_key = "HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port 0\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0";
    LPCSTR reg_value = "Identifier";

    if (is_vm_cpu = cpu_check() == 1) {
        std::cout << "This is a VM: [CPUID]: " << is_vm_cpu << std::endl;
    }
    
    DWORD known_process = vm_known_process_check(vbox_exe);
    printf("vm_known_process(%s) found PID %d", vbox_exe.c_str(), known_process);

    if (disk_check = scsi_port_check(scsi_disk, disk_key, reg_value) == 1) {
        std::cout << "This is an VBOX hard disk" << std::endl;
    }

    // Check PEB.BeingDebugged Flag
    std:cout << "Checking PEB.BeingDebugged: " << std::endl;
    int peb_set = peb_check();
    
    bool dbg = false;
    dbg = IsDebuggerPresent();
    if (dbg != 0) {
        printf("IsDebuggerPresent returned true!\n");
    }
    else {
        printf("IsDebuggerPresent returned False!\n");
    }

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
