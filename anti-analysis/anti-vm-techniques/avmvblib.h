#include <windows.h>
#include <windef.h>
#include <string>
#include <iostream>
#include <winreg.h>
#include <tlhelp32.h>
#include <tchar.h> //useful for printing information from CreateProcessToolhelp32 function
#include <stdio.h>
#include "avmvblib-utils.h"

/*  checks the ret val of CPUID.With EAX = 1 as input, the return value describes the processors features.
    If the 31st bit is 0, it is on a physical machine. Otherwise it will be 1.

    Accepts no arguments
*/
int cpu_check()
{
    bool is_under_vm = false;
    //cout << "Checking cpuid instruction..." << is_under_vm << endl; //comment out after it works
    __asm (
        xor     eax, eax
        inc     eax
        cpuid
        bt      ecx, 0x1f
        jc      under_vm
        jmp     not_under_vm
        not_under_vm :
        jmp     nop_instruction
            under_vm :
        mov     is_under_vm, 0x1
        nop_instruction :
            nop
    )
    if (is_under_vm == 1) {
        return 1;
    }
    else {
        return 0;
    }
}

/*
    Checks Registry Values for VirtualBox/VMWare settings
    1. \HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\Scsi\Scsi Port 0\Scsi Bus 0\Target Id 0\Logical Unit Id 0
*/
int scsi_port_check(char * compare, LPCSTR disk_key, LPCSTR reg_value) {
    char res[255] = { NULL };
    DWORD buffer_size = 8192;

    RegGetValueA(HKEY_LOCAL_MACHINE, disk_key, reg_value, RRF_RT_ANY, NULL, &res, &buffer_size);
    printf("RegGetValue returned: %s\n", &res);

    if (strcmp(res, compare) == 0) {
        // Replace line with handling action
        //std::cout << "Error handling and termination would happen here because REG_SV value 'VBOX    HARDDISK' was found" << std::endl;
        return 1;
    }
    return 0;
}

/*
    Checks Processes for VBox/VMware processes with tlhelp32.h
    1. VBoxService.exe
    2. VBoxTray.exe
*/
DWORD vm_known_process_check(std::string process, char* target_module = NULL) {
    std::cout << "In 'vm_known_proces_check(" << process << ")" << std::endl;

    if (target_module) {
        printf("Attempting to find module '%s'\n", target_module);
        
        //convert from char* to std::string
        const char* n_target_module = target_module;
        std::string str(n_target_module);

        // set up structure to store threads
        THREADENTRY32 thread_entry32;
        thread_entry32.dwSize = sizeof(THREADENTRY32);

        // get target pid from argv[1]
        DWORD target_pid = walk_process(process);
        if (target_pid) {
            printf("found target PID at [%d]\n", target_pid);
        };

        // enumerate threads from target process and print information
        walk_threads(target_pid);

        // enumerate modules from target process and print information
        walk_process_modules(target_pid, n_target_module);
        return target_pid;
    };

    // set up structure to store threads
    THREADENTRY32 thread_entry32;
    thread_entry32.dwSize = sizeof(THREADENTRY32);

    // get target pid from argv[1]
    DWORD target_pid = walk_process(process);
    if (target_pid) printf("found target PID at [%d]\n", target_pid);

    return target_pid;
}

/*
Checks the MAC address. Virtual Machines typically have a default MAC address
mac_check(unsigned int address)

Returns 1 on a match
See:
1. https://docs.microsoft.com/en-us/windows/win32/api/iptypes/ns-iptypes-ip_adapter_addresses_lh
2. https://docs.microsoft.com/en-us/answers/questions/331769/getting-mac-address-using-getadaptersaddresses.html
*/

/*
    Check the PEB.BeingDebugged flag - Simple manual implementation. No obfuscation
*/
int peb_check() {
    int flag = 0;
    __asm (
        xor     edx,edx
        mov     eax,fs:0x30
        cmp     byte ptr [eax+02], dl
        jne     peb_set
        peb_not_set:
            xor     eax,eax
            mov     flag,eax
            jmp     cont
        peb_set:
            xor eax, eax
            mov     flag, 0x50
        cont:
            nop
    )
    printf("Manaul debug flag was set to: %d!\n", flag);
}
